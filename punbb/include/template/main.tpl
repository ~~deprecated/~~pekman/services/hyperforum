<!DOCTYPE html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- forum_head -->
</head>
<body>
	<!-- arch navbar -->
	<div id="archnavbar" class="anb-home">
        <div id="archnavbarlogo"><h1><a href="https://www.hyperbola.info/" title="Hyperbola GNU/Linux-Libre">Hyperbola GNU/Linux-Libre</a></h1></div>
        <div id="archnavbarmenu">
            <ul id="archnavbarlist">
                <li id="anb-home"><a href="https://www.hyperbola.info/" title="Hyperbola GNU/Linux-libre">Home</a></li>
                <li id="anb-packages"><a href="https://www.hyperbola.info/packages/" title="Hyperbola GNU/Linux-libre Package Database">Packages</a></li>
                <li id="anb-forums"><a href="https://forum.hyperbola.info/" title="HyperForum | Community Support">Forums</a></li>
                <li id="anb-wiki"><a href="https://wiki.hyperbola.info/" title="HyperWiki | Community Documentation">Wiki</a></li>
                <li id="anb-issues"><a href="https://issues.hyperbola.info/" title="Report and Track on Issues">Issues</a></li>
		<li id="anb-security"><a href="https://security.hyperbola.info/" title="Hyperbola Security  Tracker">Security</a></li>
		<li id="anb-git"><a href="https://git.hyperbola.info:50100/" title="Git Projects Code">Git</a></li>
		<li id="anb-download"><a href="https://www.hyperbola.info/download/" title="Get Hyperbola">Download</a></li>
            </ul>
        </div>
    </div>
	<!-- forum_messages -->
	<div id="brd-wrap" class="brd">
	<div <!-- forum_page -->>
	<div id="brd-head" class="gen-content">
		<!-- forum_skip -->
		<!-- forum_title -->
		<!-- forum_desc -->
	</div>
	<div id="brd-navlinks" class="gen-content">
		<!-- forum_navlinks -->
		<!-- forum_admod -->
	</div>
	<div id="brd-visit" class="gen-content">
		<!-- forum_welcome -->
		<!-- forum_visit -->
	</div>
	<!-- forum_announcement -->
	<div class="hr"><hr /></div>
	<div id="brd-main">
		<!-- forum_main_title -->
		<!-- forum_crumbs_top -->
		<!-- forum_main_menu -->
		<!-- forum_main_pagepost_top -->
		<!-- forum_main -->
		<!-- forum_main_pagepost_end -->
		<!-- forum_crumbs_end -->
	</div>
		<!-- forum_qpost -->
		<!-- forum_info -->
	<div class="hr"><hr /></div>
	<div id="brd-about">
		<!-- forum_about -->
	</div>
		<!-- forum_debug -->
	</div>
	</div>
	<!-- forum_javascript -->
	<script>
	    var main_menu = responsiveNav("#brd-navlinks", {
		label: "<!-- forum_board_title -->"
	    });
	    if(document.getElementsByClassName('admin-menu').length){
		var admin_menu = responsiveNav(".admin-menu", {
		    label: "<!-- forum_lang_menu_admin -->"
		});
	    }
	    if(document.getElementsByClassName('main-menu').length){
		var profile_menu = responsiveNav(".main-menu", {
		    label: "<!-- forum_lang_menu_profile -->"
		});
	    }
	</script>
</body>
</html>
